package main.java.zus;

/**
 * Created by RENT on 2017-08-10.
 */
public class TicketGenerator {
    private final static TicketGenerator ticketGenerator = new TicketGenerator();
    private int ticketCounter = 0;

    public static TicketGenerator getInstance() {
        return ticketGenerator;
    }

    private TicketGenerator() {
    }

    public String generateTicket() {
        ticketCounter += 1;
        return "Ticket num: " + ticketCounter;
    }
}
