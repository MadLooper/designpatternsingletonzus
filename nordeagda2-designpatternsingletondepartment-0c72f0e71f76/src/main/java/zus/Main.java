package main.java.zus;

/**
 * Created by RENT on 2017-08-10.
 */
public class Main {
    public static void main(String[] args) {
        CashRegistry cashRegistry = new CashRegistry();
        HealthDepartment healthDepartment = new HealthDepartment();
        WaitingRoom waitingRoom = new WaitingRoom();
        while (true){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(cashRegistry.registerTicket());
            System.out.println(healthDepartment.healthTicket());
            System.out.println(waitingRoom.automaticTicket());
        }
    }
}
